const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config()

// mongoose.connect('mongodb+srv://dbAdmin:'
//     +process.env.MONGO_ATLAS_PWD
//     +'@bharatquiz-2z4q3.mongodb.net/test?retryWrites=true&w=majority', 
//     {
//         useNewUrlParser: true,
//         useUnifiedTopology: true
//     }
// )

mongoose.connect('mongodb://dbAdmin:'
    +process.env.MONGO_ATLAS_PWD
    +'@bharatquiz-shard-00-00-2z4q3.mongodb.net:27017,bharatquiz-shard-00-01-2z4q3.mongodb.net:27017,bharatquiz-shard-00-02-2z4q3.mongodb.net:27017/test?ssl=true&replicaSet=bharatQuiz-shard-0&authSource=admin&retryWrites=true&w=majority', 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
)

module.exports = mongoose



