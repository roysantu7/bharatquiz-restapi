const express = require('express')
const router = express.Router()

const mongoose = require('../../connection')
const Categories = require('../model/categories-model')

router.get('/', (req, res, next) => {
    Categories.find()
    .select('_id name')
    .exec()
    .then((doc) => {
        if(doc) {
            res.status(200).json({
                count: doc.length,
                data: doc
            })
        }
        else {
            res.status(404).json({
                message: "No Data Found!"
            })
        }
    })
    .catch((err) => {
        res.status(404).json({
            error: err
        })
    });
    
});

router.get('/:id', (req, res, next) => {
    const _id = req.params.id
    Categories.findById(_id)
    .select('_id name')
    .exec()
    .then((doc) => {
        if(doc) {
            res.status(200).json({
                message: "Success",
                data: doc
            })
        }
        else {
            res.status(404).json({
                message: "No Data Found!"
            })
        }
    })
    .catch((err) => {
        res.status(404).json({
            error: {
                message: err.message
            }
        })
    });
    
});

router.post('/', (req, res, next) =>{
    const category = new Categories({
        _id: mongoose.Types.ObjectId(),
        name: req.body.name
 
    })

    category.save()
    .then((result) => {
        console.log(result);
        res.status(200).json({
            message: 'Category Created',
            data: result
        })
    })
    .catch((err) => {
        res.status(500).json({
            error: err
        })
    });
})

router.delete('/:id', (req, res, next) => {
    const _id = req.params.id

    Categories.findById(_id)
    .exec()
    .then((doc) => {
        if(doc) {
            Categories.remove({_id: _id})
            .exec()
            .then((result) => {
                res.status(200)
                res.json({
                    message: "Success",
                    data: result
                })
            })
        }
        else {
            res.status(404)
            res.json({
                message: "Category Not Found!"
            })
        }
    })
    .catch((err) => {
        res.status(500).json({
            error: {
                message: err.message
            }
        })
    });
        
});


module.exports = router