const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const mongoose = require('../library/connection')
const Admin = require('../model/admin-model')


router.post('/login', (req, res, next) =>{
    Admin.find({email:req.body.email})
        .exec()
        .then( admin => {
            if(admin.length < 1) {
                res.status(404).json({
                    message: "Auth Failed! Check With Correct Credentials."
                })
            } 
            else {
                bcrypt.compare(req.body.password, admin[0].password, (err, result) => {
                    if (err){
                        res.status(500).json({
                            Error: "Auth Failed! Check With Correct Credentials."
                        })
                    }
                    else if(!result) {
                        res.status(404).json({
                            Error: "Auth Failed! Check With Correct Credentials."
                        })
                    }
                    else {
                        const token = jwt.sign(
                            {
                                email: admin[0].email,
                                _id: admin[0]._id
                            },
                            process.env.JWT_HASH,
                            {
                                expiresIn: "1h"
                            }
                        )
                        res.status(200).json({
                            message: "Auth Success!",
                            token: token,
                            data: admin
                        })
                    }
                })
            }
        })
        .catch( error => {
            res.status(500).json({
                error: error
            })
        })

})

router.post('/signup', (req, res, next) =>{
    bcrypt.hash(req.body.password, 10, (error, hash) => {
        if(error) {
            return res.status(500).json({
                Error: "Something went Wrong! please try again later."
            })
        }
        else {
            Admin.find({email:req.body.email})
            .exec()
            .then( result => {
                if(result.length < 1) {
                    const admin = new Admin({
                        _id: mongoose.Types.ObjectId(),
                        name: req.body.name,
                        email: req.body.email,
                        password: hash
                    })
                    admin.save()
                    .then((doc) => {
                        console.log(doc);
                        res.status(200).json({
                            message: 'New Admin Created',
                            data: doc
                        })
                    })
                    .catch((e) => {
                        res.status(500).json({
                            error: e
                        })
                    });
                    
                }
                else {
                    res.status(409).json({
                        message: "Email Already Exists"
                    })
                }
            })
            .catch((err) => {
                res.status(500).json({
                    error: err
                })
            });
        }
    })    
})

router.delete('/:id', (req, res, next) => {
    const _id = req.params.id

    Admin.findById(_id)
    .exec()
    .then((doc) => {
        if(doc) {
            Admin.remove({_id: _id})
            .exec()
            .then((result) => {
                res.status(200)
                res.json({
                    message: "Success",
                    data: result
                })
            })
        }
        else {
            res.status(404)
            res.json({
                message: "Category Not Found!"
            })
        }
    })
    .catch((err) => {
        res.status(500).json({
            error: {
                message: err.message
            }
        })
    });
        
});


module.exports = router