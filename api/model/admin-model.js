const mongoose = require('mongoose')

const adminSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        firstname: {
            type: String,
            required: true
        },
        lastname: {
            type: String,
            required: true
        }
        
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    type: {
        type: String,
        enum : ['manager', 'content creator', 'coordinator'],
        default: 'manager',
    }
})

module.exports = mongoose.model('Admin', adminSchema);