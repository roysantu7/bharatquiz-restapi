const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config()

mongoose.connect('mongodb+srv://'
    +process.env.MONGO_ATLAS_USER
    +':'
    +process.env.MONGO_ATLAS_PWD
    +'@bharatquiz-2z4q3.mongodb.net/test?retryWrites=true&w=majority', 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
)

module.exports = mongoose