const express = require('express')
const app = express()

const CategoriesRoutes = require('./api/routes/categories')

app.use(express.json())

let allowCrossDomain = function (req,res,next) 
{
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Methods',
        'POST',
        'GET',
        'PATCH',
        'PUT',
        'DELETE',
        'OPTIONS'
    );
    res.header(
        'Access-Control-Allow-Headers',
        'Origin,Content-Type,Accept,Authorization,session-id'
    );
    res.header(
        'Access-Control-Allow-Credentials', true
    );
    res.header(
        'Content-Length', 200
    );
    res.removeHeader("X-Powered-By");
    next();
};

app.use(allowCrossDomain);

app.use('/categories', CategoriesRoutes)

app.use((req,res,next) => {
    const error = new Error("Invalid Resource Path")
    error.status = 404
    next(error)
})

app.use((error,req,res,next) => {
    res.status(error.status || 500)
    res.json({
        error: {
            code: error.status,
            message: error.message
        }
    })
})


module.exports = app;